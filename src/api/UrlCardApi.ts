import axios, { AxiosInstance, AxiosResponse } from "axios";
import UrlCard from "@/types/UrlCard";
import ServerResponse from "@/types/ServerResponse";

const instance: AxiosInstance = axios.create({
  // Создаем объект AXIOS и задаем URL и Timeout запроса
  baseURL: "http://localhost:8080/api",
  timeout: 1500,
});

const responseBody = (response: AxiosResponse) => response.data; // Задаем формат ответа от сервера в response

const requests = {
  get: (url: string) => instance.get(url).then(responseBody),
  put: (url: string, body: UrlCard) =>
    instance.put(url, body).then(responseBody),
  delete: (url: string) => instance.delete(url).then(responseBody), // Объявляем методы с типизацией параметров и ответа
};

export const UrlCardApi = {
  // Объявляем внешний объект для взаимодействия с конкретными методами этого API.
  getUrls: (): Promise<UrlCard[]> => requests.get("urls"),
  putUrl: (urlCard: UrlCard): Promise<ServerResponse> =>
    requests.put(`urls/${urlCard.url}`, urlCard),
  deleteUrl: (url: string): Promise<ServerResponse> =>
    requests.delete(`urls/${url}`),
};
