export default interface UrlCard {
  name: string;
  description: string;
  alias: string;
  url: string;
  updated?: boolean;
}
